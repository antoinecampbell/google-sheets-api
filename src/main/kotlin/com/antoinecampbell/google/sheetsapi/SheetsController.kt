package com.antoinecampbell.google.sheetsapi

import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.model.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
@RequestMapping("/sheets")
class SheetsController(
    private val driveService: Drive,
    private val sheetsService: Sheets,
    @Value("\${drive.folder.id}") private val driveFolderId: String
) {

    @PostMapping("/create")
    fun createSpreadsheet(): ResponseEntity<*> {
        val filename = "spreadsheet-${Instant.now()}"
        // Create empty spreadsheet in Google drive folder
        val createdFile = driveService.Files().create(
            File().apply {
                name = filename
                mimeType = "application/vnd.google-apps.spreadsheet"
                parents = listOf(driveFolderId)
            }).apply {
            fields = "id, parents"
        }.execute()

        // Get spreadsheet id from created file
        val spreadsheetId = createdFile.id

        // Rename default worksheet and add additional worksheet
        sheetsService.Spreadsheets().batchUpdate(spreadsheetId,
            BatchUpdateSpreadsheetRequest().apply {
                requests = listOf(
                    Request().apply {
                        updateSheetProperties = UpdateSheetPropertiesRequest()
                            .apply {
                                properties = SheetProperties().apply {
                                    index = 0
                                    title = "Sheet A"
                                }
                                fields = "title"
                            }
                    },
                    Request().apply {
                        addSheet = AddSheetRequest().apply {
                            properties = SheetProperties().apply {
                                title = "Sheet B"
                            }
                        }
                    }
                )
            }
        ).execute()

        // Write some data to sheet A
        val sheetAData = mutableListOf<List<*>>()
        sheetAData.add(listOf("name", "count"))
        sheetAData.add(listOf("test 1", 7))
        sheetAData.add(listOf("test 2", 5))
        appendRowsToSheet(spreadsheetId, "Sheet A!A1:B${sheetAData.size}", sheetAData)

        // Write some data to sheet B
        val sheetBData = mutableListOf<List<*>>()
        sheetBData.add(listOf("title", "count"))
        sheetBData.add(listOf("test 1", 3))
        sheetBData.add(listOf("test 2", 4))
        appendRowsToSheet(spreadsheetId, "Sheet B!A1:B${sheetAData.size}", sheetBData)

        return ResponseEntity.ok(mapOf("filename" to filename))
    }

    private fun appendRowsToSheet(spreadsheetId: String, range: String, values: List<List<*>>) {
        sheetsService.spreadsheets().values()
            .append(spreadsheetId, range, ValueRange().apply {
                this.range = range
                setValues(values)
            })
            .setValueInputOption("USER_ENTERED")
            .execute()
    }
}