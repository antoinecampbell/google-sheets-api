package com.antoinecampbell.google.sheetsapi.config

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.sheets.v4.Sheets
import com.google.api.services.sheets.v4.SheetsScopes
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.GoogleCredentials
import com.google.auth.oauth2.ServiceAccountCredentials
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GoogleConfig {

    @Bean
    fun googleCredential(): GoogleCredentials = ServiceAccountCredentials.getApplicationDefault()
        .createScoped(
            DriveScopes.DRIVE_FILE,
            SheetsScopes.SPREADSHEETS,
        )

    @Bean
    fun jsonFactory(): JsonFactory = JacksonFactory()

    @Bean
    fun httpTransport(): HttpTransport = GoogleNetHttpTransport.newTrustedTransport()

    @Bean
    fun sheetsService(
        httpTransport: HttpTransport,
        jsonFactory: JsonFactory,
        googleCredentials: GoogleCredentials
    ): Sheets = Sheets(httpTransport, jsonFactory, HttpCredentialsAdapter(googleCredentials))

    @Bean
    fun driveService(
        httpTransport: HttpTransport,
        jsonFactory: JsonFactory,
        googleCredentials: GoogleCredentials
    ): Drive = Drive(httpTransport, jsonFactory, HttpCredentialsAdapter(googleCredentials))
}