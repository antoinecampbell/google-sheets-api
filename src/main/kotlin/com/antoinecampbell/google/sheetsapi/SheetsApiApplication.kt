package com.antoinecampbell.google.sheetsapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SheetsApiApplication

fun main(args: Array<String>) {
	runApplication<SheetsApiApplication>(*args)
}
