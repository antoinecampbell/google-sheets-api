import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.4.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.4.32"
    kotlin("plugin.spring") version "1.4.32"
}

group = "com.antoinecampbell.google"
java.sourceCompatibility = JavaVersion.VERSION_15


repositories {
    mavenCentral()
}

val googleAuthVersion = "0.25.5"
val googleSheetsApiVersion = "v4-rev20210504-1.31.0"
val googleDriveApiVersion = "v3-rev20210509-1.31.0"

dependencies {
    // Spring boot
    implementation("org.springframework.boot:spring-boot-starter-web")
    // Jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    // Google
    implementation("com.google.auth:google-auth-library-oauth2-http:$googleAuthVersion")
    implementation("com.google.apis:google-api-services-sheets:$googleSheetsApiVersion")
    implementation("com.google.apis:google-api-services-drive:$googleDriveApiVersion")

    // Testing
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "15"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
